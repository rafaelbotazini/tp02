﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP02
{
    class Program
    {
        static void Main(string[] args)
        {
            bool menu = true;
            while (menu)
            {
                Console.Clear();

                Console.WriteLine("\tEscolha um programa digitando seu número correspondente e pressionando <Enter>\n");
                Console.WriteLine("1) Característica do número de 4 algarismos");
                Console.WriteLine("2) Número Perfeito");
                Console.WriteLine("3) Advinha o número!");
                Console.WriteLine("4) Intercalação de vetores");
                Console.WriteLine("5) Produto escalar");
                Console.WriteLine("0) Sair");

                int opcao = int.Parse(Console.ReadLine());

                Console.Clear();

                switch (opcao)
                {
                    case 0:
                        {
                            menu = false;
                        }
                        break;

                    case 1:
                        {
                            Console.WriteLine("Dos números 1000 a 9999, sendo 'a', 'b', 'c' e 'd' respectivamente" +
                                " cada um de seus algarismos, aqueles x, tais que x = (ab + cd)² são:");

                            for (int i = 1000; i < 1e4; i++)
                            {
                                // primeiro par
                                int dezena = (i / 1000) * 10;
                                int a = dezena + i % 1000 / 100;

                                // segundo par
                                dezena = (i % 1000 % 100 / 10) * 10;
                                int b = dezena + i % 1000 % 100 % 10;

                                if (Math.Pow((a + b), 2) == i)
                                    Console.WriteLine(i);
                            }
                        }
                        break;

                    case 2:
                        {
                            Console.WriteLine("Os números perfeitos de 1 a 100 são: ");

                            for (int i = 1; i <= 100; i++)
                            {
                                int divisores = 0;
                                string soma = "";

                                for (int j = 1; j < i; j++)
                                {
                                    if (i % j == 0)
                                    {
                                        divisores += j;
                                        soma += j + " + ";
                                    }
                                }
                                if (soma.Length > 0)
                                    soma = soma.Remove(soma.LastIndexOf('+'));

                                if (divisores == i)
                                {
                                    Console.WriteLine("{0}, pois {1} = {2}", i, divisores, soma);
                                }
                            }
                        }
                        break;

                    case 3:
                        {
                            var random = new Random();

                            int r = random.Next(1, 100);
                            int num = 0;
                            int chutes = 0;

                            Console.WriteLine("Estou pensando em um número de 1 a 100.\nVocê consegue adivinhá-lo?");

                            while (num != r)
                            {
                                Console.Write("\nChute um número: ");
                                num = int.Parse(Console.ReadLine());

                                chutes++;

                                if (r < num)
                                    Console.WriteLine("Mais baixo!");
                                else if (r > num)
                                    Console.WriteLine("Mais alto!");
                            }

                            Console.Write("\nAcertou ");

                            if (chutes == 1)
                                Console.WriteLine("de primeira!? Sorte... HAHA!");
                            else if (chutes < 10)
                                Console.WriteLine("na segunda tentativa! Bom trabalho! :D");
                            else if (chutes < 20)
                                Console.WriteLine("em {0} tentativas. Nada mal.", chutes);
                            else
                                Console.WriteLine("em {0} tentativas.", chutes);

                        }
                        break;

                    case 4:
                        {
                            Console.WriteLine("Defina duas listas de 20 valores cada.");
                            Console.Write("Ao final da edição, serão mostrados os valores destas listas intercalados");

                            int[] lista1 = new int[20];
                            int[] lista2 = new int[20];

                            int[] todos = new int[40];

                            Console.WriteLine("\nInsira 20 números inteiros para a primeira lista: ");
                            lista1 = ObterListaInt(20);

                            Console.WriteLine("\nInsira 20 números inteiros para a segunda lista: ");
                            lista2 = ObterListaInt(20);

                            bool par = true;
                            int index = 0;

                            Console.WriteLine("\nLista intercalada: ");
                            for (int i = 0; i < 40; i++)
                            {
                                if (par)
                                {
                                    todos[i] = lista1[index];
                                }
                                else
                                {
                                    todos[i] = lista2[index];
                                    index++;
                                }

                                Console.Write("L{0} | ", par ? 1 : 2);
                                Console.WriteLine(todos[i]);

                                par = !par;

                            }
                        }
                        break;

                    case 5:
                        {
                            Console.Write("Defina duas listas, A e B, cada uma contendo 15 valores.");
                            Console.WriteLine("Será calculado o produto escalar entre estes dois conjuntos de valores.");

                            int[] a = new int[20];
                            int[] b = new int[20];
                            int produto = 0;

                            Console.WriteLine("\nInsira 15 números inteiros para a lista A: ");
                            a = ObterListaInt(15);

                            Console.WriteLine("\nInsira 15 números inteiros para a lista B: ");
                            b = ObterListaInt(15);

                            for (int i = 0; i < 15; i++)
                            {
                                produto += a[i] * b[i];
                            }

                            Console.WriteLine("O produto escalar entre A e B é " + produto);
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Opção inválida.");
                            break;
                        }
                }

                if (opcao != 0)
                {
                    Console.WriteLine("\nPressione qualquer tecla para retornar ao menu");
                    Console.ReadKey(true);
                }
            }
        }

        static int[] ObterListaInt(int length)
        {
            int[] arr = new int[length];

            for (int i = 0; i < length; i++)
            {
                Console.Write("item {0}: ", i + 1);
                arr[i] = int.Parse(Console.ReadLine());
            }

            return arr;
        }
    }
}
